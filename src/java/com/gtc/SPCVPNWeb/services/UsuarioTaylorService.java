package com.gtc.SPCVPNWeb.services;

import java.util.List;

import com.gtc.SPCVPNWeb.controller.UsuarioTaylorDAO;
import com.gtc.SPCVPNWeb.controller.UsuariosDAO;
import com.gtc.SPCVPNWeb.model.Usuario;
import com.gtc.SPCVPNWeb.model.Usuariotaylo;


/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class UsuarioTaylorService {
	public static  final String TABLE_PARAM_NOMBREUSUARIO="NOMBREUSUARIO";
	
	private static UsuarioTaylorDAO usuariosTylorDAO;
	public UsuarioTaylorService() {
		usuariosTylorDAO=new UsuarioTaylorDAO(); 
	}

	public void persist(Usuariotaylo entity) {
		usuariosTylorDAO.openCurrentSessionwithTransaction();
		usuariosTylorDAO.persist(entity);
		usuariosTylorDAO.closeCurrentSessionwithTransaction();
	}

	public void update(Usuariotaylo entity) {
		usuariosTylorDAO.openCurrentSessionwithTransaction();
		usuariosTylorDAO.update(entity);
		usuariosTylorDAO.closeCurrentSessionwithTransaction();
	}

	public Usuariotaylo findById(String id) {
		usuariosTylorDAO.openCurrentSession();
		Usuariotaylo  usuario = usuariosTylorDAO.findById(id);
		usuariosTylorDAO.closeCurrentSession();
		return usuario;
	}
	
	public Usuariotaylo findByNombreUsuario(String nombreUsuario) {
		usuariosTylorDAO.openCurrentSession();
		Usuariotaylo  usuario = usuariosTylorDAO.findByStringParameter("NOMBREUSUARIO", nombreUsuario) ;
		usuariosTylorDAO.closeCurrentSession();
		return usuario;
	}

	public void delete(String id) {
		usuariosTylorDAO.openCurrentSessionwithTransaction();
		Usuariotaylo usuarios = usuariosTylorDAO.findById(id);
		usuariosTylorDAO.delete(usuarios);
		usuariosTylorDAO.closeCurrentSessionwithTransaction();
	}

	public List<Usuariotaylo> findAll() {
		usuariosTylorDAO.openCurrentSession();
		List<Usuariotaylo> usuarios = usuariosTylorDAO.findAll();
		usuariosTylorDAO.closeCurrentSession();
		return usuarios;
	}

	public void deleteAll() {
		usuariosTylorDAO.openCurrentSessionwithTransaction();
		usuariosTylorDAO.deleteAll();
		usuariosTylorDAO.closeCurrentSessionwithTransaction();
	}

	public UsuarioTaylorDAO usuarioDao() {
		return usuariosTylorDAO;
	}
	
	/*public static void main(String[] Args){
		
		UsuarioTaylorService usuarioService=new UsuarioTaylorService();
		System.out.println(usuarioService.findById("1"));
		//System.out.println(usuarioService.findByNombreUsuario("Haimer"));
		
	}*/
	
}
