package com.gtc.SPCVPNWeb.services;

import java.util.List;

import com.gtc.SPCVPNWeb.controller.EstadoDAO;
import com.gtc.SPCVPNWeb.controller.UsuariosDAO;
import com.gtc.SPCVPNWeb.model.Estado;
import com.gtc.SPCVPNWeb.model.Usuario;


/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class EstadoService {
	public static  final String TABLE_PARAM_NOMBREUSUARIO="NOMBREUSUARIO";
	
	private static EstadoDAO estadoDAO;
	public EstadoService() {
		estadoDAO=new EstadoDAO();
	}

	public void persist(Estado entity) {
		estadoDAO.openCurrentSessionwithTransaction();
		estadoDAO.persist(entity);
		estadoDAO.closeCurrentSessionwithTransaction();
	}

	public void update(Estado entity) {
		estadoDAO.openCurrentSessionwithTransaction();
		estadoDAO.update(entity);
		estadoDAO.closeCurrentSessionwithTransaction();
	}

	public Estado findById(String id) {
		estadoDAO.openCurrentSession();
		Estado  estado = estadoDAO.findById(id);
		estadoDAO.closeCurrentSession();
		return estado;
	}
	
	public Estado findByNombreUsuario(String nombreUsuario) {
		estadoDAO.openCurrentSession();
		Estado  estado = estadoDAO.findByStringParameter("NOMBREUSUARIO", nombreUsuario) ;
		estadoDAO.closeCurrentSession();
		return estado;
	}

	public void delete(String id) {
		estadoDAO.openCurrentSessionwithTransaction();
		Estado estado = estadoDAO.findById(id);
		estadoDAO.delete(estado);
		estadoDAO.closeCurrentSessionwithTransaction();
	}

	public List<Estado> findAll() {
		estadoDAO.openCurrentSession();
		List<Estado> estados = estadoDAO.findAll();
		estadoDAO.closeCurrentSession();
		return estados;
	}

	public void deleteAll() {
		estadoDAO.openCurrentSessionwithTransaction();
		estadoDAO.deleteAll();
		estadoDAO.closeCurrentSessionwithTransaction();
	}

	public EstadoDAO estadoDao() {
		return estadoDAO;
	}
	
	
}
