package com.gtc.SPCVPNWeb.services;

import java.util.List;

import com.gtc.SPCVPNWeb.controller.UsuariosDAO;
import com.gtc.SPCVPNWeb.model.Usuario;


/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class UsuarioService {
	public static  final String TABLE_PARAM_NOMBREUSUARIO="NOMBREUSUARIO";
	
	private static UsuariosDAO usuariosDAO;
	public UsuarioService() {
		usuariosDAO=new UsuariosDAO();
	}

	public void persist(Usuario entity) {
		usuariosDAO.openCurrentSessionwithTransaction();
		usuariosDAO.persist(entity);
		usuariosDAO.closeCurrentSessionwithTransaction();
	}

	public void update(Usuario entity) {
		usuariosDAO.openCurrentSessionwithTransaction();
		usuariosDAO.update(entity);
		usuariosDAO.closeCurrentSessionwithTransaction();
	}

	public Usuario findById(String id) {
		usuariosDAO.openCurrentSession();
		Usuario  usuario = usuariosDAO.findById(id);
		usuariosDAO.closeCurrentSession();
		return usuario;
	}
	
	public Usuario findByNombreUsuario(String nombreUsuario) {
		usuariosDAO.openCurrentSession();
		Usuario  usuario = usuariosDAO.findByStringParameter("NOMBREUSUARIO", nombreUsuario) ;
		usuariosDAO.closeCurrentSession();
		return usuario;
	}

	public void delete(String id) {
		usuariosDAO.openCurrentSessionwithTransaction();
		Usuario usuarios = usuariosDAO.findById(id);
		usuariosDAO.delete(usuarios);
		usuariosDAO.closeCurrentSessionwithTransaction();
	}

	public List<Usuario> findAll() {
		usuariosDAO.openCurrentSession();
		List<Usuario> usuarios = usuariosDAO.findAll();
		usuariosDAO.closeCurrentSession();
		return usuarios;
	}

	public void deleteAll() {
		usuariosDAO.openCurrentSessionwithTransaction();
		usuariosDAO.deleteAll();
		usuariosDAO.closeCurrentSessionwithTransaction();
	}

	public UsuariosDAO usuarioDao() {
		return usuariosDAO;
	}
	
	/*public static void main(String[] Args){
		
		UsuarioService usuarioService=new UsuarioService();
		System.out.println(usuarioService.findById("1"));
		//System.out.println(usuarioService.findByNombreUsuario("Haimer"));
		
	}*/
	
}
