package com.gtc.SPCVPNWeb.services;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.gtc.SPCVPNWeb.controller.AplicacionDAO;
import com.gtc.SPCVPNWeb.controller.FormularioPruebaDAO;
import com.gtc.SPCVPNWeb.controller.UsuariosDAO;
import com.gtc.SPCVPNWeb.model.Aplicacion;
import com.gtc.SPCVPNWeb.model.Estado;
import com.gtc.SPCVPNWeb.model.Formularioprueba;
import com.gtc.SPCVPNWeb.model.Resultado;
import com.gtc.SPCVPNWeb.model.Usuario;


/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class FormularioPruebaService {
	
	public static  final String TABLE_PARAM_NOMBREUSUARIO="NOMBREUSUARIO";
	
	
	private AplicacionDAO aplicacionDAO;
	
	private FormularioPruebaDAO formularioPruebaDAO;
	
	public FormularioPruebaService() {
		aplicacionDAO=new AplicacionDAO();
		formularioPruebaDAO=new FormularioPruebaDAO();
	}


	public void persist(Aplicacion entity) {
		aplicacionDAO.openCurrentSessionwithTransaction();
		aplicacionDAO.persist(entity);
		aplicacionDAO.closeCurrentSessionwithTransaction();
	}

	public void update(Aplicacion entity) {
		aplicacionDAO.openCurrentSessionwithTransaction();
		aplicacionDAO.update(entity);
		aplicacionDAO.closeCurrentSessionwithTransaction();
	}

	public Aplicacion findById(String id) {
		aplicacionDAO.openCurrentSession();
		Aplicacion  aplicacion = aplicacionDAO.findById(id);
		aplicacionDAO.closeCurrentSession();
		return aplicacion;
	}
	
	public Aplicacion findByNombreAplicacion(String nombreUsuario) {
		aplicacionDAO.openCurrentSession();
		Aplicacion  aplicacion = aplicacionDAO.findByStringParameter("NOMBRE", nombreUsuario) ;
		aplicacionDAO.closeCurrentSession();
		return aplicacion;
	}

	public void delete(String id) {
		aplicacionDAO.openCurrentSessionwithTransaction();
		Aplicacion aplicacion = aplicacionDAO.findById(id);
		aplicacionDAO.delete(aplicacion);
		aplicacionDAO.closeCurrentSessionwithTransaction();
	}

	public List<Formularioprueba> findAll() {
		formularioPruebaDAO.openCurrentSession();
		List<Formularioprueba> formularios = formularioPruebaDAO.findAll();
		formularioPruebaDAO.closeCurrentSession();
		return formularios;
	}

	public void deleteAll() {
		aplicacionDAO.openCurrentSessionwithTransaction();
		aplicacionDAO.deleteAll();
		aplicacionDAO.closeCurrentSessionwithTransaction();
	}

	public AplicacionDAO usuarioDao() {
		return aplicacionDAO;
	}
	
	
	
}
