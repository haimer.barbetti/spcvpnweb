/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "NOTIFICACION")
@XmlRootElement
public class Notificacion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Lob
    @Column(name = "FONDO")
    private Serializable fondo;
    @Basic(optional = false)
    @Column(name = "TIPO_NOTIFICACION")
    private String tipoNotificacion;
    @Column(name = "PERIOCIDAD")
    private Long periocidad;
    @Column(name = "CANT_DIAS")
    private Long cantDias;
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @Column(name = "HORACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horacreacion;
    @Column(name = "USUARIOCREADOR")
    private String usuariocreador;
    @Column(name = "FECHAMODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamodificacion;
    @Column(name = "HORAMODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horamodificacion;
    @Column(name = "USUARIOMODIFICADOR")
    private String usuariomodificador;
    @Column(name = "GUARDADOPARCIAL")
    private String guardadoparcial;
    @JoinColumn(name = "ESTADO_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Estado estadoId;

    public Notificacion() {
    }

    public Notificacion(Long id) {
        this.id = id;
    }

    public Notificacion(Long id, String tipoNotificacion) {
        this.id = id;
        this.tipoNotificacion = tipoNotificacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Serializable getFondo() {
        return fondo;
    }

    public void setFondo(Serializable fondo) {
        this.fondo = fondo;
    }

    public String getTipoNotificacion() {
        return tipoNotificacion;
    }

    public void setTipoNotificacion(String tipoNotificacion) {
        this.tipoNotificacion = tipoNotificacion;
    }

    public Long getPeriocidad() {
        return periocidad;
    }

    public void setPeriocidad(Long periocidad) {
        this.periocidad = periocidad;
    }

    public Long getCantDias() {
        return cantDias;
    }

    public void setCantDias(Long cantDias) {
        this.cantDias = cantDias;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Date getHoracreacion() {
        return horacreacion;
    }

    public void setHoracreacion(Date horacreacion) {
        this.horacreacion = horacreacion;
    }

    public String getUsuariocreador() {
        return usuariocreador;
    }

    public void setUsuariocreador(String usuariocreador) {
        this.usuariocreador = usuariocreador;
    }

    public Date getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechamodificacion(Date fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }

    public Date getHoramodificacion() {
        return horamodificacion;
    }

    public void setHoramodificacion(Date horamodificacion) {
        this.horamodificacion = horamodificacion;
    }

    public String getUsuariomodificador() {
        return usuariomodificador;
    }

    public void setUsuariomodificador(String usuariomodificador) {
        this.usuariomodificador = usuariomodificador;
    }

    public String getGuardadoparcial() {
        return guardadoparcial;
    }

    public void setGuardadoparcial(String guardadoparcial) {
        this.guardadoparcial = guardadoparcial;
    }

    public Estado getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(Estado estadoId) {
        this.estadoId = estadoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.Notificacion[ id=" + id + " ]";
    }
    
}
