/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Gestecon_df_carvajal
 */
@Embeddable
public class NotificacionFormulariopruebaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "NOTIFICACION_ID")
    private long notificacionId;
    @Basic(optional = false)
    @Column(name = "FORMULARIO_PRUEBA_ID")
    private long formularioPruebaId;

    public NotificacionFormulariopruebaPK() {
    }

    public NotificacionFormulariopruebaPK(long notificacionId, long formularioPruebaId) {
        this.notificacionId = notificacionId;
        this.formularioPruebaId = formularioPruebaId;
    }

    public long getNotificacionId() {
        return notificacionId;
    }

    public void setNotificacionId(long notificacionId) {
        this.notificacionId = notificacionId;
    }

    public long getFormularioPruebaId() {
        return formularioPruebaId;
    }

    public void setFormularioPruebaId(long formularioPruebaId) {
        this.formularioPruebaId = formularioPruebaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) notificacionId;
        hash += (int) formularioPruebaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionFormulariopruebaPK)) {
            return false;
        }
        NotificacionFormulariopruebaPK other = (NotificacionFormulariopruebaPK) object;
        if (this.notificacionId != other.notificacionId) {
            return false;
        }
        if (this.formularioPruebaId != other.formularioPruebaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.NotificacionFormulariopruebaPK[ notificacionId=" + notificacionId + ", formularioPruebaId=" + formularioPruebaId + " ]";
    }
    
}
