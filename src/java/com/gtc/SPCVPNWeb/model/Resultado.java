/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "RESULTADO")
@XmlRootElement
public class Resultado implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ResultadoPK resultadoPK;
    @Column(name = "DESCRIPCION")
    private String descripcion;
    @Column(name = "OPCION")
    private String opcion;
    @JoinColumn(name = "APLICACION_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Aplicacion aplicacion;
    @JoinColumn(name = "FALLA_ID", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Fallas fallaId;

    public Resultado() {
    }

    public Resultado(ResultadoPK resultadoPK) {
        this.resultadoPK = resultadoPK;
    }

    public Resultado(long id, BigInteger aplicacionId) {
        this.resultadoPK = new ResultadoPK(id, aplicacionId);
    }

    public ResultadoPK getResultadoPK() {
        return resultadoPK;
    }

    public void setResultadoPK(ResultadoPK resultadoPK) {
        this.resultadoPK = resultadoPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getOpcion() {
        return opcion;
    }

    public void setOpcion(String opcion) {
        this.opcion = opcion;
    }

    public Aplicacion getAplicacion() {
        return aplicacion;
    }

    public void setAplicacion(Aplicacion aplicacion) {
        this.aplicacion = aplicacion;
    }

    public Fallas getFallaId() {
        return fallaId;
    }

    public void setFallaId(Fallas fallaId) {
        this.fallaId = fallaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (resultadoPK != null ? resultadoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Resultado)) {
            return false;
        }
        Resultado other = (Resultado) object;
        if ((this.resultadoPK == null && other.resultadoPK != null) || (this.resultadoPK != null && !this.resultadoPK.equals(other.resultadoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.Resultado[ resultadoPK=" + resultadoPK + " ]";
    }
    
}
