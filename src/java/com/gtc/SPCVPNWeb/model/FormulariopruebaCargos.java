/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "FORMULARIOPRUEBA_CARGOS")
@XmlRootElement
public class FormulariopruebaCargos implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected FormulariopruebaCargosPK formulariopruebaCargosPK;
    @JoinColumn(name = "CARGO_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.EAGER)
    private Cargo cargo;

    public FormulariopruebaCargos() {
    }

    public FormulariopruebaCargos(FormulariopruebaCargosPK formulariopruebaCargosPK) {
        this.formulariopruebaCargosPK = formulariopruebaCargosPK;
    }

    public FormulariopruebaCargos(long formularioPruebaId, long cargoId) {
        this.formulariopruebaCargosPK = new FormulariopruebaCargosPK(formularioPruebaId, cargoId);
    }

    public FormulariopruebaCargosPK getFormulariopruebaCargosPK() {
        return formulariopruebaCargosPK;
    }

    public void setFormulariopruebaCargosPK(FormulariopruebaCargosPK formulariopruebaCargosPK) {
        this.formulariopruebaCargosPK = formulariopruebaCargosPK;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formulariopruebaCargosPK != null ? formulariopruebaCargosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormulariopruebaCargos)) {
            return false;
        }
        FormulariopruebaCargos other = (FormulariopruebaCargos) object;
        if ((this.formulariopruebaCargosPK == null && other.formulariopruebaCargosPK != null) || (this.formulariopruebaCargosPK != null && !this.formulariopruebaCargosPK.equals(other.formulariopruebaCargosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.FormulariopruebaCargos[ formulariopruebaCargosPK=" + formulariopruebaCargosPK + " ]";
    }
    
}
