/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "VPN")
@XmlRootElement
public class Vpn implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USUARIO")
    private String usuario;
    @Column(name = "NOMBRE_COLABORADOR")
    private String nombreColaborador;
    @Column(name = "VENCIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vencimiento;
    @Column(name = "CREADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creado;
    @Column(name = "DESDE_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date desdeHora;
    @Column(name = "HASTA_HORA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hastaHora;

    public Vpn() {
    }

    public Vpn(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getNombreColaborador() {
        return nombreColaborador;
    }

    public void setNombreColaborador(String nombreColaborador) {
        this.nombreColaborador = nombreColaborador;
    }

    public Date getVencimiento() {
        return vencimiento;
    }

    public void setVencimiento(Date vencimiento) {
        this.vencimiento = vencimiento;
    }

    public Date getCreado() {
        return creado;
    }

    public void setCreado(Date creado) {
        this.creado = creado;
    }

    public Date getDesdeHora() {
        return desdeHora;
    }

    public void setDesdeHora(Date desdeHora) {
        this.desdeHora = desdeHora;
    }

    public Date getHastaHora() {
        return hastaHora;
    }

    public void setHastaHora(Date hastaHora) {
        this.hastaHora = hastaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vpn)) {
            return false;
        }
        Vpn other = (Vpn) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.Vpn[ id=" + id + " ]";
    }
    
}
