/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Gestecon_df_carvajal
 */
@Embeddable
public class FormulariopruebaCargosPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "FORMULARIO_PRUEBA_ID")
    private long formularioPruebaId;
    @Basic(optional = false)
    @Column(name = "CARGO_ID")
    private long cargoId;

    public FormulariopruebaCargosPK() {
    }

    public FormulariopruebaCargosPK(long formularioPruebaId, long cargoId) {
        this.formularioPruebaId = formularioPruebaId;
        this.cargoId = cargoId;
    }

    public long getFormularioPruebaId() {
        return formularioPruebaId;
    }

    public void setFormularioPruebaId(long formularioPruebaId) {
        this.formularioPruebaId = formularioPruebaId;
    }

    public long getCargoId() {
        return cargoId;
    }

    public void setCargoId(long cargoId) {
        this.cargoId = cargoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) formularioPruebaId;
        hash += (int) cargoId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormulariopruebaCargosPK)) {
            return false;
        }
        FormulariopruebaCargosPK other = (FormulariopruebaCargosPK) object;
        if (this.formularioPruebaId != other.formularioPruebaId) {
            return false;
        }
        if (this.cargoId != other.cargoId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.FormulariopruebaCargosPK[ formularioPruebaId=" + formularioPruebaId + ", cargoId=" + cargoId + " ]";
    }
    
}
