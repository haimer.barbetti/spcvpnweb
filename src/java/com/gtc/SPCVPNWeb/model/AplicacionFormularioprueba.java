/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "APLICACION_FORMULARIOPRUEBA")
@XmlRootElement
public class AplicacionFormularioprueba implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AplicacionFormulariopruebaPK aplicacionFormulariopruebaPK;

    public AplicacionFormularioprueba() {
    }

    public AplicacionFormularioprueba(AplicacionFormulariopruebaPK aplicacionFormulariopruebaPK) {
        this.aplicacionFormulariopruebaPK = aplicacionFormulariopruebaPK;
    }

    public AplicacionFormularioprueba(long aplicacionId, long formularioPruebaId) {
        this.aplicacionFormulariopruebaPK = new AplicacionFormulariopruebaPK(aplicacionId, formularioPruebaId);
    }

    public AplicacionFormulariopruebaPK getAplicacionFormulariopruebaPK() {
        return aplicacionFormulariopruebaPK;
    }

    public void setAplicacionFormulariopruebaPK(AplicacionFormulariopruebaPK aplicacionFormulariopruebaPK) {
        this.aplicacionFormulariopruebaPK = aplicacionFormulariopruebaPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (aplicacionFormulariopruebaPK != null ? aplicacionFormulariopruebaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AplicacionFormularioprueba)) {
            return false;
        }
        AplicacionFormularioprueba other = (AplicacionFormularioprueba) object;
        if ((this.aplicacionFormulariopruebaPK == null && other.aplicacionFormulariopruebaPK != null) || (this.aplicacionFormulariopruebaPK != null && !this.aplicacionFormulariopruebaPK.equals(other.aplicacionFormulariopruebaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.AplicacionFormularioprueba[ aplicacionFormulariopruebaPK=" + aplicacionFormulariopruebaPK + " ]";
    }
    
}
