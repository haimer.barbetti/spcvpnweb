/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Gestecon_df_carvajal
 */
@Embeddable
public class AplicacionFormulariopruebaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "APLICACION_ID")
    private long aplicacionId;
    @Basic(optional = false)
    @Column(name = "FORMULARIO_PRUEBA_ID")
    private long formularioPruebaId;

    public AplicacionFormulariopruebaPK() {
    }

    public AplicacionFormulariopruebaPK(long aplicacionId, long formularioPruebaId) {
        this.aplicacionId = aplicacionId;
        this.formularioPruebaId = formularioPruebaId;
    }

    public long getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(long aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    public long getFormularioPruebaId() {
        return formularioPruebaId;
    }

    public void setFormularioPruebaId(long formularioPruebaId) {
        this.formularioPruebaId = formularioPruebaId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) aplicacionId;
        hash += (int) formularioPruebaId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AplicacionFormulariopruebaPK)) {
            return false;
        }
        AplicacionFormulariopruebaPK other = (AplicacionFormulariopruebaPK) object;
        if (this.aplicacionId != other.aplicacionId) {
            return false;
        }
        if (this.formularioPruebaId != other.formularioPruebaId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.AplicacionFormulariopruebaPK[ aplicacionId=" + aplicacionId + ", formularioPruebaId=" + formularioPruebaId + " ]";
    }
    
}
