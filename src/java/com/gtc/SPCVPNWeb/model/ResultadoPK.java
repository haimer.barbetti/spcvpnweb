/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Gestecon_df_carvajal
 */
@Embeddable
public class ResultadoPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ID")
    private long id;
    @Basic(optional = false)
    @Column(name = "APLICACION_ID")
    private BigInteger aplicacionId;

    public ResultadoPK() {
    }

    public ResultadoPK(long id, BigInteger aplicacionId) {
        this.id = id;
        this.aplicacionId = aplicacionId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigInteger getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(BigInteger aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (aplicacionId != null ? aplicacionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ResultadoPK)) {
            return false;
        }
        ResultadoPK other = (ResultadoPK) object;
        if (this.id != other.id) {
            return false;
        }
        if ((this.aplicacionId == null && other.aplicacionId != null) || (this.aplicacionId != null && !this.aplicacionId.equals(other.aplicacionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.ResultadoPK[ id=" + id + ", aplicacionId=" + aplicacionId + " ]";
    }
    
}
