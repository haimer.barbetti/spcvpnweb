/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "NOTIFICACION_FORMULARIOPRUEBA")
@XmlRootElement
public class NotificacionFormularioprueba implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected NotificacionFormulariopruebaPK notificacionFormulariopruebaPK;

    public NotificacionFormularioprueba() {
    }

    public NotificacionFormularioprueba(NotificacionFormulariopruebaPK notificacionFormulariopruebaPK) {
        this.notificacionFormulariopruebaPK = notificacionFormulariopruebaPK;
    }

    public NotificacionFormularioprueba(long notificacionId, long formularioPruebaId) {
        this.notificacionFormulariopruebaPK = new NotificacionFormulariopruebaPK(notificacionId, formularioPruebaId);
    }

    public NotificacionFormulariopruebaPK getNotificacionFormulariopruebaPK() {
        return notificacionFormulariopruebaPK;
    }

    public void setNotificacionFormulariopruebaPK(NotificacionFormulariopruebaPK notificacionFormulariopruebaPK) {
        this.notificacionFormulariopruebaPK = notificacionFormulariopruebaPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (notificacionFormulariopruebaPK != null ? notificacionFormulariopruebaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionFormularioprueba)) {
            return false;
        }
        NotificacionFormularioprueba other = (NotificacionFormularioprueba) object;
        if ((this.notificacionFormulariopruebaPK == null && other.notificacionFormulariopruebaPK != null) || (this.notificacionFormulariopruebaPK != null && !this.notificacionFormulariopruebaPK.equals(other.notificacionFormulariopruebaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.NotificacionFormularioprueba[ notificacionFormulariopruebaPK=" + notificacionFormulariopruebaPK + " ]";
    }
    
}
