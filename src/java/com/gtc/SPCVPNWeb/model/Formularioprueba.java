/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gtc.SPCVPNWeb.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Gestecon_df_carvajal
 */
@MappedSuperclass
@Table(name = "FORMULARIOPRUEBA")
@XmlRootElement
public class Formularioprueba implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOMBRE")
    private String nombre;
    @Column(name = "NOTIFICACION_ID")
    private Long notificacionId;
    @Basic(optional = false)
    @Column(name = "ESTADO_ID")
    private long estadoId;
    @Basic(optional = false)
    @Column(name = "TIPOFORMULARIO_ID")
    private long tipoformularioId;
    @Column(name = "DURACION")
    private Long duracion;
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @Column(name = "HORACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horacreacion;
    @Column(name = "USUARIOCREADOR")
    private String usuariocreador;
    @Column(name = "FECHAMODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechamodificacion;
    @Column(name = "HORAMODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date horamodificacion;
    @Column(name = "USUARIOMODIFICADOR")
    private String usuariomodificador;
    @Column(name = "GUARDADOPARCIAL")
    private String guardadoparcial;
    @Column(name = "ESTADO_DILIGENCIA")
    private String estadoDiligencia;
    @Column(name = "FECHA_ELABORACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaElaboracion;
    @Basic(optional = false)
    @Column(name = "APLICACION ID")
    private BigInteger aplicacionId;

    public Formularioprueba() {
    }

    public Formularioprueba(Long id) {
        this.id = id;
    }

    public Formularioprueba(Long id, long estadoId, long tipoformularioId, BigInteger aplicacionId) {
        this.id = id;
        this.estadoId = estadoId;
        this.tipoformularioId = tipoformularioId;
        this.aplicacionId = aplicacionId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Long getNotificacionId() {
        return notificacionId;
    }

    public void setNotificacionId(Long notificacionId) {
        this.notificacionId = notificacionId;
    }

    public long getEstadoId() {
        return estadoId;
    }

    public void setEstadoId(long estadoId) {
        this.estadoId = estadoId;
    }

    public long getTipoformularioId() {
        return tipoformularioId;
    }

    public void setTipoformularioId(long tipoformularioId) {
        this.tipoformularioId = tipoformularioId;
    }

    public Long getDuracion() {
        return duracion;
    }

    public void setDuracion(Long duracion) {
        this.duracion = duracion;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Date getHoracreacion() {
        return horacreacion;
    }

    public void setHoracreacion(Date horacreacion) {
        this.horacreacion = horacreacion;
    }

    public String getUsuariocreador() {
        return usuariocreador;
    }

    public void setUsuariocreador(String usuariocreador) {
        this.usuariocreador = usuariocreador;
    }

    public Date getFechamodificacion() {
        return fechamodificacion;
    }

    public void setFechamodificacion(Date fechamodificacion) {
        this.fechamodificacion = fechamodificacion;
    }

    public Date getHoramodificacion() {
        return horamodificacion;
    }

    public void setHoramodificacion(Date horamodificacion) {
        this.horamodificacion = horamodificacion;
    }

    public String getUsuariomodificador() {
        return usuariomodificador;
    }

    public void setUsuariomodificador(String usuariomodificador) {
        this.usuariomodificador = usuariomodificador;
    }

    public String getGuardadoparcial() {
        return guardadoparcial;
    }

    public void setGuardadoparcial(String guardadoparcial) {
        this.guardadoparcial = guardadoparcial;
    }

    public String getEstadoDiligencia() {
        return estadoDiligencia;
    }

    public void setEstadoDiligencia(String estadoDiligencia) {
        this.estadoDiligencia = estadoDiligencia;
    }

    public Date getFechaElaboracion() {
        return fechaElaboracion;
    }

    public void setFechaElaboracion(Date fechaElaboracion) {
        this.fechaElaboracion = fechaElaboracion;
    }

    public BigInteger getAplicacionId() {
        return aplicacionId;
    }

    public void setAplicacionId(BigInteger aplicacionId) {
        this.aplicacionId = aplicacionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formularioprueba)) {
            return false;
        }
        Formularioprueba other = (Formularioprueba) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gtc.SPCVPNWeb.model.Formularioprueba[ id=" + id + " ]";
    }
    
}
