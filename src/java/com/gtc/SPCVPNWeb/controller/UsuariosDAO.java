package com.gtc.SPCVPNWeb.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.gtc.SPCVPNWeb.controller.generic.DAO;
import com.gtc.SPCVPNWeb.model.Usuario;

/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class UsuariosDAO implements DAO<Usuario, String, String> {

	private Session currentSession;
	private Transaction currentTransaction;

	public UsuariosDAO() {

	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void persist(Usuario entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void update(Usuario entity) {
		getCurrentSession().update(entity);
	}

	@Override
	public Usuario findById(String id) {
		Usuario usuario = (Usuario) getCurrentSession().get(Usuario.class,
				Long.parseLong(id));
		return usuario;
	}

	@Override
	public Usuario findByStringParameter(String nameParameter, String nombre) {
		Usuario usuario = (Usuario) getCurrentSession().createQuery("from Usuario where " + nameParameter + " ='" + nombre + "'").uniqueResult();
		return usuario;
	}

	@Override
	public void delete(Usuario entity) {
		getCurrentSession().delete(entity);
	}
	
	@Override
	public void deleteAll() {
		List<Usuario> entityList = findAll();
		for (Usuario entity : entityList) {
			delete(entity);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Usuario> findAll() {
		 List<Usuario> usuarios = (List<Usuario>) getCurrentSession().createQuery("from Usuario").list();
		 return new ArrayList<Usuario>();

	}

}
