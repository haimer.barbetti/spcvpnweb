package com.gtc.SPCVPNWeb.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.gtc.SPCVPNWeb.controller.generic.DAO;
import com.gtc.SPCVPNWeb.model.Estado;
import com.gtc.SPCVPNWeb.model.Usuario;

/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class EstadoDAO implements DAO<Estado, String, String> {

	private Session currentSession;
	private Transaction currentTransaction;

	public EstadoDAO() {}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void persist(Estado entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void update(Estado entity) {
		getCurrentSession().update(entity);
	}

	@Override
	public Estado findById(String id) {
		Estado estado= (Estado) getCurrentSession().get(Estado.class,Long.parseLong(id));
		return estado;
	}

	@Override
	public Estado findByStringParameter(String nameParameter, String nombre) {
		Estado estado = (Estado) getCurrentSession().createQuery("from Estado where " + nameParameter + " ='" + nombre + "'").uniqueResult();
		return estado;
	}

	@Override
	public void delete(Estado entity) {
		getCurrentSession().delete(entity);
	}
	
	@Override
	public void deleteAll() {
		List<Estado> entityList = findAll();
		for (Estado entity : entityList) {
			delete(entity);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Estado> findAll() {
		 List<Estado> estado = (List<Estado>) getCurrentSession().createQuery("from Estado").list();
		 return new ArrayList<Estado>();

	}

}
