package com.gtc.SPCVPNWeb.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.gtc.SPCVPNWeb.controller.generic.DAO;
import com.gtc.SPCVPNWeb.model.Usuario;
import com.gtc.SPCVPNWeb.model.Usuariotaylo;

/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class UsuarioTaylorDAO implements DAO<Usuariotaylo, String, String> {

	private Session currentSession;
	private Transaction currentTransaction;

	public UsuarioTaylorDAO() {

	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration
				.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void persist(Usuariotaylo entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void update(Usuariotaylo entity) {
		getCurrentSession().update(entity);
	}

	@Override
	public Usuariotaylo findById(String id) {
		Usuariotaylo usuario = (Usuariotaylo) getCurrentSession().get(Usuario.class,
				Long.parseLong(id));
		return usuario;
	}

	@Override
	public Usuariotaylo findByStringParameter(String nameParameter, String nombre) {
		Usuariotaylo usuario = (Usuariotaylo) getCurrentSession().createQuery("from Usuariotaylo where " + nameParameter + " ='" + nombre + "'").uniqueResult();
		return usuario;
	}

	@Override
	public void delete(Usuariotaylo entity) {
		getCurrentSession().delete(entity);
	}
	
	@Override
	public void deleteAll() {
		List<Usuariotaylo> entityList = findAll();
		for (Usuariotaylo entity : entityList) {
			delete(entity);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Usuariotaylo> findAll() {
		 List<Usuariotaylo> usuarios = (List<Usuariotaylo>) getCurrentSession().createQuery("from Usuariotaylo").list();
		 return new ArrayList<Usuariotaylo>();

	}

}
