package com.gtc.SPCVPNWeb.controller;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.gtc.SPCVPNWeb.controller.generic.DAO;
import com.gtc.SPCVPNWeb.model.Aplicacion;
import com.gtc.SPCVPNWeb.model.Usuario;

/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class AplicacionDAO implements DAO<Aplicacion, String, String> {

	private Session currentSession;
	private Transaction currentTransaction;

	public AplicacionDAO() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void persist(Aplicacion entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void update(Aplicacion entity) {
		getCurrentSession().update(entity);
	}

	@Override
	public Aplicacion findById(String id) {
		Aplicacion aplicacion= (Aplicacion) getCurrentSession().get(Aplicacion.class,Long.parseLong(id));
			return aplicacion;
	}

	@Override
	public Aplicacion findByStringParameter(String nameParameter, String nombre) {
		Aplicacion aplicacion = (Aplicacion) getCurrentSession().createQuery("from Aplicacion where " + nameParameter + " ='" + nombre + "'").uniqueResult();
		return aplicacion;
	}

	@Override
	public void delete(Aplicacion entity) {
		getCurrentSession().delete(entity);
	}
	
	@Override
	public void deleteAll() {
		List<Aplicacion> entityList = findAll();
		for (Aplicacion entity : entityList) {
			delete(entity);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Aplicacion> findAll() {
		 List<Aplicacion> usuarios = (List<Aplicacion>) getCurrentSession().createQuery("from Aplicacion").list();
		 return new ArrayList<Aplicacion>();
	}

}
