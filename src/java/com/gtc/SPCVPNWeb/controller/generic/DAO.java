package com.gtc.SPCVPNWeb.controller.generic;

import java.io.Serializable;
import java.util.List;

/***
 * 
 * @author Haimer Barbetti
 * @param <T>
 * @since 12/07/2017
 * 
 */
public interface DAO<T, Id,NameParameter extends Serializable> {
	public void persist(T entity);

	public void update(T entity);

	public T findById(Id id);
	
	public T findByStringParameter(NameParameter nameParameter,Id id);
	
	public void delete(T entity);

	public List<T> findAll();

	public void deleteAll();
}
