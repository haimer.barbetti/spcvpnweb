package com.gtc.SPCVPNWeb.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.gtc.SPCVPNWeb.controller.generic.DAO;
import com.gtc.SPCVPNWeb.model.Aplicacion;
import com.gtc.SPCVPNWeb.model.Formularioprueba;
import com.gtc.SPCVPNWeb.model.Usuario;
import java.math.BigInteger;

/***
 * 
 * @author Haimer Barbetti
 * @since 12/07/2017
 * 
 */
public class FormularioPruebaDAO implements DAO<Formularioprueba, String, String> {

	private Session currentSession;
	private Transaction currentTransaction;

	public FormularioPruebaDAO() {
	}

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	private static SessionFactory getSessionFactory() {
		Configuration configuration = new Configuration().configure();
		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
		SessionFactory sessionFactory = configuration.buildSessionFactory(builder.build());
		return sessionFactory;
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	@Override
	public void persist(Formularioprueba entity) {
		getCurrentSession().save(entity);
	}

	@Override
	public void update(Formularioprueba entity) {
		getCurrentSession().update(entity);
	}

	@Override
	public Formularioprueba findById(String id) {
		Formularioprueba formularioprueba= (Formularioprueba) getCurrentSession().get(Formularioprueba.class,Long.parseLong(id));
			return formularioprueba;
	}

	@Override
	public Formularioprueba findByStringParameter(String nameParameter, String nombre) {
		Formularioprueba formularioprueba = (Formularioprueba) getCurrentSession().createQuery("from Formularioprueba where " + nameParameter + " ='" + nombre + "'").uniqueResult();
		return formularioprueba;
	}

	@Override
	public void delete(Formularioprueba entity) {
		getCurrentSession().delete(entity);
	}
	
	@Override
	public void deleteAll() {
		List<Formularioprueba> entityList = findAll();
		for (Formularioprueba entity : entityList) {
			delete(entity);
		}
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Formularioprueba> findAll() {
		 //List<Formularioprueba> formularioprueba = (List<Formularioprueba>) getCurrentSession().createQuery("from Formularioprueba").list();
		List<Formularioprueba> formularioprueba =new ArrayList<Formularioprueba>();
		
		SQLQuery query=getCurrentSession().createSQLQuery("Select * from FORMULARIOPRUEBA");
		List<Object[]> entities = query.list();
		
		Formularioprueba fm=new Formularioprueba();
		
		for (Object[] entity : entities) {
	        for (Object entityCol : entity) {
	        	
	        	//fm=(Formularioprueba)entityCol;
	        	
	        	System.out.println( ((Formularioprueba)entityCol).getNombre() );
	        	
	        	fm.setId(  ((Formularioprueba)entityCol).getId() );
	        	fm.setNombre(((Formularioprueba)entityCol).getNombre() );
	        	fm.setAplicacionId(  (BigInteger)((Formularioprueba)entityCol).getAplicacionId()  );
	        	formularioprueba.add(fm);
	        }
	        
	    }
		//List<Formularioprueba> formularioprueba = (List<Formularioprueba>) getCurrentSession().createSQLQuery("Select * from FORMULARIOPRUEBA").list();
		return formularioprueba;
	}
	
	
}
